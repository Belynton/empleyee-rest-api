﻿using Microsoft.AspNetCore.Mvc;
using MyApplication.dto;
using MyApplication.Service;

namespace MyApplication.Controller;

[Route("api/[controller]/")]
[ApiController]
public class EmployeeController : ControllerBase
{
    private readonly IEmployeeService _employeeService;

    public EmployeeController(IEmployeeService employeeService)
    {
        _employeeService = employeeService;
    }


    // GET api/<EmployeeController>/department/5
    [HttpGet("department/{departmentName}")]
    public async Task<IActionResult> GetWithDepartmentName(string departmentName)
    {
        var employees = await _employeeService.FindAllWhereDepartmentName(departmentName);
        return Ok(employees);
    }

    // GET api/<EmployeeController>/company/5
    [HttpGet("company/{companyId}")]
    public async Task<IActionResult> GetWithCompanyId(int companyId)
    {
        var employees = await _employeeService.FindAllWhereCompanyId(companyId);
        return Ok(employees);
    }

    // POST api/<EmployeeController>
    [HttpPost]
    public async Task<IActionResult> Post([FromBody] EmployeeCreateRqDto employeeRqDto)
    {
        var result = await _employeeService.Create(employeeRqDto);
        return Ok(result);
    }

    // PUT api/<EmployeeController>/5
    [HttpPut("{id}")]
    public async Task<IActionResult> Update(int id, [FromBody] EmployeeUpdateRqDto employeeUpdateRqDto)
    {
        var result = await _employeeService.Update(id, employeeUpdateRqDto);
        return Ok(result);
    }

    // DELETE api/<EmployeeController>/5
    [HttpDelete("{id}")]
    public async Task<IActionResult> Delete(int id)
    {
        await _employeeService.Delete(id);
        return Accepted(202);
    }
}