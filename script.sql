CREATE SCHEMA if not exists public;

CREATE TABLE if not exists public.department
(
    id      SERIAL                  PRIMARY KEY,
    name    character varying(20)   NOT NULL,
    phone   character varying(12)   NOT NULL
);

CREATE TABLE if not exists public.passport
(
    id      SERIAL                  PRIMARY KEY,
    type    character varying(15)   NOT NULL,
    number  character varying(6)    NOT NULL
);

CREATE TABLE if not exists public.employee
(
    id              SERIAL                  PRIMARY KEY,
    name            character varying(20)   NOT NULL,
    surname         character varying(20)   NOT NULL,
    phone           character varying(12)   NOT NULL,
    company_id      integer                 NOT NULL,
    passport_id     SERIAL                  NOT NULL,
    department_id   SERIAL                  NOT NULL,
    CONSTRAINT employee_fk FOREIGN KEY(department_id) REFERENCES public.department(id),
    CONSTRAINT employee_fk2 FOREIGN KEY(passport_id) REFERENCES public.passport(id)
);