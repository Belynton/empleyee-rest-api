﻿using System.Data;
using MyApplication.dto;
using MyApplication.Entity;

namespace MyApplication.Repository;

public interface IDepartmentRepository
{
    Task<Department> FindById(int id);

    Task<int> Create(DepartmentCreateRqDto departmentRqDto, IDbConnection connection);

    Task Delete(int id, IDbConnection connection);

    Task Update(int id, DepartmentUpdateRqDto departmentRqDto, IDbConnection connection);
}