﻿using MyApplication.dto;
using MyApplication.Entity;

namespace MyApplication.Repository;

public interface IEmployeeRepository
{
    Task<IEnumerable<Employee>> FindAllWhereDepartmentName(string departmentName);

    Task<IEnumerable<Employee>> FindAllWhereCompanyId(int id);

    Task<int> Create(EmployeeCreateRqDto employeeRqDto);

    Task<int> Update(int employeeId, int departmentId, int passportId, EmployeeUpdateRqDto employeeRqDto);

    Task<Employee?> FindById(int id);

    Task Delete(int employeeId, int departmentId, int passportId);
}