﻿using System.Data;
using Dapper;
using MyApplication.Context;
using MyApplication.dto;
using MyApplication.Entity;

namespace MyApplication.Repository.Impl;

public class PassportRepository : IPassportRepository
{
    private readonly DapperContext _context;

    public PassportRepository(DapperContext context)
    {
        _context = context;
    }

    public async Task<int> Create(PassportCreateRqDto passportRqDto, IDbConnection connection)
    {
        var query = $"INSERT INTO passport (\"type\", \"number\") VALUES(@Type, @Number) RETURNING id";
        var returnedId = await connection.ExecuteScalarAsync<int>(query, passportRqDto);
        return returnedId;
    }

    public async Task Delete(int id, IDbConnection connection)
    {
        var query = $"DELETE FROM passport WHERE id={id}";
        await connection.ExecuteAsync(query);
    }
    

    public async Task Update(int id, PassportUpdateRqDto passportRqDto, IDbConnection connection)
    {
        var query = $"UPDATE passport SET number = @Number, type = @Type " +
                    $"WHERE id = {id}";
        await connection.ExecuteAsync(query, passportRqDto);
    }

    public async Task<Passport> FindById(int id)
    {
        var query = $"SELECT \"type\", \"number\", id FROM passport WHERE id={id}";
        using var connection = _context.CreateConnection();
        var passport = await connection.QueryFirstAsync<Passport>(query);
        return passport;
    }
}