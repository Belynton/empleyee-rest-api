﻿using Dapper;
using MyApplication.Context;
using MyApplication.dto;
using MyApplication.Entity;

namespace MyApplication.Repository.Impl;

public class EmployeeRepository : IEmployeeRepository
{
    private readonly DapperContext _context;
    private readonly IDepartmentRepository _departmentRepository;
    private readonly IPassportRepository _passportRepository;

    public EmployeeRepository(DapperContext context, IDepartmentRepository departmentRepository,
        IPassportRepository passportRepository)
    {
        _context = context;
        _departmentRepository = departmentRepository;
        _passportRepository = passportRepository;
    }

    public async Task<int> Create(EmployeeCreateRqDto employeeRqDto)
    {
        using var connection = _context.CreateConnection();
        connection.Open();
        using var transaction = connection.BeginTransaction();
        var departmentId = await _departmentRepository.Create(employeeRqDto.Department, connection);
        var passportId = await _passportRepository.Create(employeeRqDto.Passport, connection);
        var query = $"INSERT INTO employee (name, surname, phone, company_id, passport_id, department_id) " +
                    $"VALUES(@Name, @Surname, @Phone, @CompanyId, @PassportId, @DepartmentId) " +
                    $"RETURNING id";
        var returnedId = await connection.ExecuteScalarAsync<int>(query, new
        {
            employeeRqDto.Name,
            employeeRqDto.Surname,
            employeeRqDto.Phone,
            employeeRqDto.CompanyId,
            departmentId,
            passportId
        });
        transaction.Commit();
        return returnedId;
    }

    public async Task Delete(int employeeId, int departmentId, int passportId)
    {
        var query = $"DELETE FROM employee WHERE id={employeeId}";
        using var connection = _context.CreateConnection();
        connection.Open();
        using var transaction = connection.BeginTransaction();
        await connection.ExecuteAsync(query);
        await _departmentRepository.Delete(departmentId, connection);
        await _passportRepository.Delete(passportId, connection);
        transaction.Commit();
    }

    public async Task<IEnumerable<Employee>> FindAllWhereCompanyId(int id)
    {
        var query = $"SELECT id, name, surname, phone, company_id AS CompanyId, " +
                    $"passport_id AS PassportId, department_id AS DepartmentId " +
                    $"FROM employee " +
                    $"WHERE company_id={id}";
        using var connection = _context.CreateConnection();
        var employees = await connection.QueryAsync<Employee>(query);
        return employees;
    }

    public async Task<IEnumerable<Employee>> FindAllWhereDepartmentName(string departmentName)
    {
        var query = $"SELECT employee.name, employee.surname, employee.phone, employee.company_id AS CompanyId, " +
                    $"employee.passport_id AS PassportId, employee.department_id AS DepartmentId, employee.id " +
                    $"FROM employee join department on employee.department_id = department.id " +
                    $"WHERE department.name='{departmentName}'";
        using var connection = _context.CreateConnection();
        var employees = await connection.QueryAsync<Employee>(query);
        return employees;
    }

    public async Task<int> Update(int employeeId, int departmentId, int passportId, EmployeeUpdateRqDto employeeRqDto)
    {
        var query = $"UPDATE employee SET \"name\" = @Name, surname = @Surname, phone = @Phone, " +
                    $"company_id = @CompanyId WHERE id = {employeeId} RETURNING id";
        using var connection = _context.CreateConnection();
        connection.Open();
        using var transaction = connection.BeginTransaction();
        var returnedId = await connection.ExecuteScalarAsync<int>(query, employeeRqDto);
        if (employeeRqDto.Department != null)
        {
            await _departmentRepository.Update(departmentId, employeeRqDto.Department, connection);
        }

        if (employeeRqDto.Passport != null)
        {
            await _passportRepository.Update(passportId, employeeRqDto.Passport, connection);
        }

        transaction.Commit();
        return returnedId;
    }

    public async Task<Employee?> FindById(int id)
    {
        var query = $"SELECT \"name\", surname, phone, company_id AS CompanyId," +
                    $" passport_id AS PassportId, department_id AS DepartmentId," +
                    $" id FROM employee WHERE id={id}";
        var connection = _context.CreateConnection();
        var employee = await connection.QueryFirstOrDefaultAsync<Employee>(query);
        return employee;
    }
}