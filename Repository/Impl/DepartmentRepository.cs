﻿using System.Data;
using Dapper;
using MyApplication.Context;
using MyApplication.dto;
using MyApplication.Entity;

namespace MyApplication.Repository.Impl;

public class DepartmentRepository : IDepartmentRepository
{
    private readonly DapperContext _context;

    public DepartmentRepository(DapperContext context)
    {
        _context = context;
    }

    public async Task<int> Create(DepartmentCreateRqDto departmentRqDto, IDbConnection connection)
    {
        var query = $"INSERT INTO department (\"name\", phone) VALUES(@Name, @Phone) RETURNING id";
        var returnedId = await connection.ExecuteScalarAsync<int>(query, departmentRqDto);
        return returnedId;
    }

    public async Task<Department> FindById(int id)
    {
        var query = $"SELECT id, name, phone FROM department WHERE id={id}";
        using var connection = _context.CreateConnection();
        var department = await connection.QueryFirstAsync<Department>(query);
        return department;
    }

    public async Task Delete(int id, IDbConnection connection)
    {
        var query = $"DELETE FROM department WHERE id={id}";
        await connection.ExecuteAsync(query);
    }

    public async Task Update(int id, DepartmentUpdateRqDto departmentRqDto, IDbConnection connection)
    {
        var query = $"UPDATE department SET \"name\" = @Name, phone = @Phone " +
                    $"WHERE id = {id}";
        await connection.ExecuteAsync(query, departmentRqDto);
    }
}