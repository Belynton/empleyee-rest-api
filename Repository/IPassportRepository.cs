﻿using System.Data;
using MyApplication.dto;
using MyApplication.Entity;

namespace MyApplication.Repository;

public interface IPassportRepository
{
    Task<Passport> FindById(int id);

    Task<int> Create(PassportCreateRqDto passportRqDto, IDbConnection connection);

    Task Delete(int id, IDbConnection connection);

    Task Update(int id, PassportUpdateRqDto passportRqDto, IDbConnection connection);
}