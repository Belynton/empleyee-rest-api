﻿using MyApplication.dto;
using MyApplication.Entity;

namespace MyApplication.Mapper;

public interface IEmployeeMapper
{
    EmployeeRsDto ToRsDto(Employee employee);
}