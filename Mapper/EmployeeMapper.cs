﻿using MyApplication.dto;
using MyApplication.Entity;
using MyApplication.Service;

namespace MyApplication.Mapper;

public class EmployeeMapper : IEmployeeMapper
{
    private readonly IDepartmentService _departmentService;
    private readonly IPassportService _passportService;

    public EmployeeMapper(IPassportService passportService, IDepartmentService departmentService)
    {
        _passportService = passportService;
        _departmentService = departmentService;
    }

    public EmployeeRsDto ToRsDto(Employee employee)
    {
        return new EmployeeRsDto
        {
            Id = employee.Id,
            Name = employee.Name,
            Surname = employee.Surname,
            Phone = employee.Phone,
            CompanyId = employee.CompanyId,
            Department = _departmentService.FindById(employee.DepartmentId).Result,
            Passport = _passportService.FindById(employee.PassportId).Result
        };
    }
}