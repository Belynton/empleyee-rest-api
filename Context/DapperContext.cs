﻿using System.Data;
using Dapper;
using Npgsql;

namespace MyApplication.Context;

public class DapperContext
{
    private readonly IConfiguration _configuration;

    public DapperContext(IConfiguration configuration)
    {
        _configuration = configuration;
        InitDatabase();
    }

    public IDbConnection CreateConnection()
        => new NpgsqlConnection(_configuration.GetConnectionString("DefaultConnection"));

    private void InitDatabase()
    {
        using var connection = CreateConnection();
        var script = File.ReadAllText(@"script.sql");
        connection.Execute(script);
    }
}