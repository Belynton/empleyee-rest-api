﻿using System.Text.Json.Serialization;

namespace MyApplication.dto;

public class PassportRsDto
{
    [JsonIgnore] public int Id { get; set; }

    public string Type { get; set; }

    public string Number { get; set; }
}