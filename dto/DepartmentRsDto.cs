﻿using System.Text.Json.Serialization;

namespace MyApplication.dto;

public class DepartmentRsDto
{
    [JsonIgnore] public int Id { get; set; }

    public string Name { get; set; }

    public string Phone { get; set; }
}