﻿namespace MyApplication.dto;

public class EmployeeCreateRqDto
{
    public string Name { get; set; }
    public string Surname { get; set; }
    public string Phone { get; set; }
    public int CompanyId { get; set; }
    public PassportCreateRqDto Passport { get; set; }
    public DepartmentCreateRqDto Department { get; set; }
}