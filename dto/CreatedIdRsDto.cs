﻿namespace MyApplication.dto;

public class CreatedIdRsDto
{

    public int Id { get; set; }
}