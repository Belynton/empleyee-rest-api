﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyApplication.Entity;

[Table("passport")]
public class Passport
{
    [Key] [Column("id")] public int Id { get; set; }

    [Column("type")] public string Type { get; set; }

    [Column("number")] public string Number { get; set; }
}