﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyApplication.Entity;

[Table("employee")]
public class Employee
{
    [Key] [Column("id")] public int Id { get; set; }

    [Column("name")] public string Name { get; set; }

    [Column("surname")] public string Surname { get; set; }

    [Column("phone")] public string Phone { get; set; }

    [Column("company_id")] public int CompanyId { get; set; }

    [Column("passport_id")] public int PassportId { get; set; }

    [Column("department_id")] public int DepartmentId { get; set; }
}