﻿using System.Text.RegularExpressions;
using MyApplication.dto;
using MyApplication.Entity;
using MyApplication.Exceptions;
using MyApplication.Repository;

namespace MyApplication.Service.Impl;

public class DepartmentService : IDepartmentService
{
    private readonly IDepartmentRepository _departmentRepository;

    public DepartmentService(IDepartmentRepository departmentRepository)
    {
        _departmentRepository = departmentRepository;
    }

    public async Task<DepartmentRsDto> FindById(int id)
    {
        var department = await _departmentRepository.FindById(id);
        ThrowIfDepartmentNotFound(id, department);
        return new DepartmentRsDto
        {
            Id = department.Id,
            Name = department.Name,
            Phone = department.Phone
        };
    }

    private static void ThrowIfDepartmentNotFound(int id, Department department)
    {
        if (department == null)
        {
            throw new NotFoundException($"Department with id = {id} not found");
        }
    }

    public void ThrowIfPhoneNotValid(string number)
    {
        var phoneTemplate = new Regex(@"^\+[1-9]{1}[0-9]{10}$");
        if (number != null && !phoneTemplate.Match(number).Success)
        {
            throw new BadRequest("Incorrect phone number.");
        }
    }

    public async Task CopyNotNullFields(int from, EmployeeUpdateRqDto to)
    {
        var department = await FindById(from);
        if (string.IsNullOrEmpty(to.Department!.Phone))
        {
            to.Department.Phone = department.Phone;
        }

        if (string.IsNullOrEmpty(to.Department.Name))
        {
            to.Department.Name = department.Name;
        }

        ThrowIfPhoneNotValid(to.Department.Phone);
    }
}