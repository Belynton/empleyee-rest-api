﻿using System.Text.RegularExpressions;
using MyApplication.dto;
using MyApplication.Entity;
using MyApplication.Exceptions;
using MyApplication.Repository;

namespace MyApplication.Service.Impl;

public class PassportService : IPassportService
{
    private readonly IPassportRepository _passportRepository;

    public PassportService(IPassportRepository passportRepository)
    {
        _passportRepository = passportRepository;
    }

    public async Task<PassportRsDto> FindById(int id)
    {
        var passport = await _passportRepository.FindById(id);
        ThrowIfPassportNotFound(id, passport);
        return new PassportRsDto
        {
            Id = passport.Id,
            Number = passport.Number,
            Type = passport.Type
        };
    }

    private void ThrowIfPassportNotFound(int id, Passport passport)
    {
        if (passport == null)
        {
            throw new NotFoundException($"Passport with id = {id} not found");
        }
    }

    public void ThrowIfNumberNotValid(string number)
    {
        var numberTemplate = new Regex(@"^[1-9]{1}[0-9]{5}$");
        if (number != null && !numberTemplate.Match(number).Success)
        {
            throw new BadRequest("Incorrect passport number.");
        }
    }

    public async Task CopyNotNullFields(int from, EmployeeUpdateRqDto to)
    {
        var passport = await FindById(from);
        if (string.IsNullOrEmpty(to.Passport!.Number))
        {
            to.Passport.Number = passport.Number;
        }

        if (string.IsNullOrEmpty(to.Passport.Type))
        {
            to.Passport.Type = passport.Type;
        }

        ThrowIfNumberNotValid(to.Passport.Number);
    }
}