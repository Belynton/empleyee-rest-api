﻿using System.Text.RegularExpressions;
using MyApplication.dto;
using MyApplication.Entity;
using MyApplication.Exceptions;
using MyApplication.Mapper;
using MyApplication.Repository;

namespace MyApplication.Service.Impl;

public class EmployeeService : IEmployeeService
{
    private readonly IEmployeeRepository _employeeRepository;
    private readonly IEmployeeMapper _employeeMapper;
    private readonly IDepartmentService _departmentService;
    private readonly IPassportService _passportService;

    public EmployeeService(IEmployeeRepository employeeRepository, IEmployeeMapper employeeMapper,
        IDepartmentService departmentService, IPassportService passportService)
    {
        _employeeRepository = employeeRepository;
        _employeeMapper = employeeMapper;
        _departmentService = departmentService;
        _passportService = passportService;
    }

    public async Task<IEnumerable<EmployeeRsDto>> FindAllWhereDepartmentName(string departmentName)
    {
        var employees = await _employeeRepository.FindAllWhereDepartmentName(departmentName);
        ThrowIfEmployeesByDepartmentNameNotFound(departmentName, employees);
        return employees.Select(employee => _employeeMapper.ToRsDto(employee));
    }

    public async Task<IEnumerable<EmployeeRsDto>> FindAllWhereCompanyId(int id)
    {
        var employees = await _employeeRepository.FindAllWhereCompanyId(id);
        ThrowIfEmployeesByCompanyIdNotFound(id, employees);
        return employees.Select(employee => _employeeMapper.ToRsDto(employee));
    }

    public async Task<CreatedIdRsDto> Create(EmployeeCreateRqDto employeeRqDto)
    {
        ValidateEmployeeCreateDto(employeeRqDto);
        var id = await _employeeRepository.Create(employeeRqDto);
        return new CreatedIdRsDto
        {
            Id = id
        };
    }

    public async Task<CreatedIdRsDto> Update(int id, EmployeeUpdateRqDto employeeRqDto)
    {
        ThrowIfPhoneNotValid(employeeRqDto.Phone);
        var employee = await _employeeRepository.FindById(id);
        ThrowIfEmployeeNotFound(employee, id);
        await CopyNotNullFields(employee!, employeeRqDto);
        ThrowIfCompanyIdNotCorrect(employeeRqDto.CompanyId);
        var returnedId =
            await _employeeRepository.Update(id, employee!.DepartmentId, employee.PassportId, employeeRqDto);
        return new CreatedIdRsDto
        {
            Id = returnedId
        };
    }

    public async Task Delete(int id)
    {
        var employee = await _employeeRepository.FindById(id);
        ThrowIfEmployeeNotFound(employee, id);
        await _employeeRepository.Delete(id, employee!.DepartmentId, employee.PassportId);
    }

    private void ValidateEmployeeCreateDto(EmployeeCreateRqDto employeeRqDto)
    {
        ThrowIfPhoneNotValid(employeeRqDto.Phone);
        ThrowIfCompanyIdNotCorrect(employeeRqDto.CompanyId);
        _departmentService.ThrowIfPhoneNotValid(employeeRqDto.Department.Phone);
        _passportService.ThrowIfNumberNotValid(employeeRqDto.Passport.Number);
    }

    private void ThrowIfCompanyIdNotCorrect(int companyId)
    {
        if (companyId <= 0)
        {
            throw new BadRequest("Incorrect company id value.");
        }
    }

    private void ThrowIfEmployeeNotFound(Employee? employee, int id)
    {
        if (employee == null)
        {
            throw new NotFoundException($"Employee with id = {id} not found!");
        }
    }

    private void ThrowIfPhoneNotValid(string number)
    {
        var phoneTemplate = new Regex(@"^\+[1-9]{1}[0-9]{10}$");
        if (number != null && !phoneTemplate.Match(number).Success)
        {
            throw new BadRequest("Incorrect phone number.");
        }
    }

    private void ThrowIfEmployeesByDepartmentNameNotFound(string departmentName, IEnumerable<Employee> employees)
    {
        if (!employees.Any())
        {
            throw new NotFoundException($"Employees with department name = {departmentName} not found");
        }
    }

    private void ThrowIfEmployeesByCompanyIdNotFound(int id, IEnumerable<Employee> employees)
    {
        if (!employees.Any())
        {
            throw new NotFoundException($"Employees with company id = {id} not found");
        }
    }

    private async Task CopyNotNullFields(Employee from, EmployeeUpdateRqDto to)
    {
        if (string.IsNullOrEmpty(to.Name))
        {
            to.Name = from.Name;
        }

        if (string.IsNullOrEmpty(to.Surname))
        {
            to.Surname = from.Surname;
        }

        if (string.IsNullOrEmpty(to.Phone))
        {
            to.Phone = from.Phone;
        }

        if (to.CompanyId == 0)
        {
            to.CompanyId = from.CompanyId;
        }

        if (to.Department != null)
        {
            await _departmentService.CopyNotNullFields(from.DepartmentId, to);
        }

        if (to.Passport != null)
        {
            await _passportService.CopyNotNullFields(from.PassportId, to);
        }
    }
}