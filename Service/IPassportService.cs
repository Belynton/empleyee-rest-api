﻿using MyApplication.dto;

namespace MyApplication.Service;

public interface IPassportService
{
    Task<PassportRsDto> FindById(int id);

    Task CopyNotNullFields(int from, EmployeeUpdateRqDto to);

    void ThrowIfNumberNotValid(string number);
}