﻿using MyApplication.dto;

namespace MyApplication.Service;

public interface IDepartmentService
{
    Task<DepartmentRsDto> FindById(int id);

    Task CopyNotNullFields(int from, EmployeeUpdateRqDto to);

    void ThrowIfPhoneNotValid(string number);
}