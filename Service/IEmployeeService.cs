﻿using MyApplication.dto;

namespace MyApplication.Service;

public interface IEmployeeService
{
    Task<IEnumerable<EmployeeRsDto>> FindAllWhereDepartmentName(string departmentName);

    Task<IEnumerable<EmployeeRsDto>> FindAllWhereCompanyId(int id);

    Task<CreatedIdRsDto> Create(EmployeeCreateRqDto employeeRqDto);

    Task<CreatedIdRsDto> Update(int id, EmployeeUpdateRqDto employeeRqDto);

    Task Delete(int id);
}